//
//  Page.swift
//  Zoomi
//
//  Created by ShakhzodUrinboev on 12/30/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit

struct Page{
    let imageName:String
    let bodyText:String
}

