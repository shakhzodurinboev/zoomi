//
//  MainViewController.swift
//  zoomy
//
//  Created by ShakhzodUrinboev on 11/30/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit
import SideMenu

class MainViewController: UIViewController {
    
    var rightBarButton: UIBarButtonItem!
    var hamburgerMenuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hamburgerSettings()
        setupIcons()
        setupTopImage(imageName: "logo")
        self.view.backgroundColor = Theme.current.viewControllerBackgroundColor
        
    }
    
    @IBAction func btnPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "BarcodeScannerStoryboard", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "BarcodeScanner")
        self.navigationController?.pushViewController(vc, animated: true)
//        self.navigationController?.pushViewController(BarcodeScanner(), animated: true)

    }
    
    func setupIcons(){
        let rightBarButton = UIBarButtonItem.init(image: UIImage.init(named: "bell_black"), style: .plain, target: self, action: #selector(showNotifications))
        self.rightBarButton = rightBarButton
        let hamburgerMenuButton = UIBarButtonItem.init(image: UIImage.init(named: "menu"), style: .plain, target: self, action: #selector(showMenu))
        self.navigationController?.navigationBar.tintColor = UIColor.short(red: 55, green: 55, blue: 55)
        self.navigationController!.navigationBar.topItem?.rightBarButtonItem = rightBarButton
        self.navigationController!.navigationBar.topItem?.leftBarButtonItem = hamburgerMenuButton
    }
    
    @objc func showNotifications(){
        print("Show notifications")
    }
    
    @objc func showMenu(){
        guard let hamburger = SideMenuManager.default.menuLeftNavigationController else { return }
        present(hamburger, animated: true, completion: nil)
    }
    private func hamburgerSettings() {
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: HamburgerMenu())
        if let hamburgerVC = menuLeftNavigationController.topViewController as? HamburgerMenu {
            hamburgerVC.delegate = self
        }
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuWidth = UIScreen.main.bounds.width
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
    }
}
extension MainViewController: HamburgerMenuDelegate {
    func buttonSelected(with tag: Int) {
        if self.children.count > 0 {
            let viewControllers:[UIViewController] = self.children
            for viewContoller in viewControllers {
                viewContoller.willMove(toParent: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParent()
            }
        }
        switch tag {
        case 0:
            //          //Profile
            add(asChildViewController: ProfileViewController(nibName: nil, bundle: nil))
        case 1:
            print(tag) // History
            add(asChildViewController: HistoryViewController(nibName: nil, bundle: nil))
        case 2: // Favorites
            print(tag)
            add(asChildViewController: FavoritesViewController(nibName: nil, bundle: nil))
        case 3: //Comments
            print(tag)
            add(asChildViewController: CommentsViewController(nibName: nil, bundle: nil))
        case 4: //Share Zoomi
            print(tag)
            
        case 5: //Language
            print(tag)
            add(asChildViewController: LanguageViewController(nibName: nil, bundle: nil))
        case 6: //About
            print(tag)
            add(asChildViewController: AboutViewController(nibName: nil, bundle: nil))
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension MainViewController {
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChild(viewController)
        // Add Child View as Subview
        view.addSubview(viewController.view)
        // Configure Child View
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
}

