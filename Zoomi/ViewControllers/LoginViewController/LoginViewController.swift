//
//  LoginViewController.swift
//  zoomy
//
//  Created by ShakhzodUrinboev on 12/9/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit
import SnapKit
import ActionSheetPicker_3_0
import Localize_Swift

class LoginViewController: UIViewController {
    
    let age = ["18-25", "25-30", "30-40", "40-50", "50-60"]
    let gender = ["MALE", "FEMALE"]

    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
       setupDesign()
    }
    @IBAction func buttonTapped(_ sender: UIButton) {
        self.present(UINavigationController(rootViewController: MainViewController()), animated: true, completion: nil)
    }
    
    func setupDesign(){
        loginButton.layer.masksToBounds = true
        loginButton.layer.cornerRadius = 20
        genderLabel.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(openGenderPickerView))
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(openAgePickerView))
        ageLabel.isUserInteractionEnabled = true
        genderLabel.addGestureRecognizer(gesture)
        ageLabel.addGestureRecognizer(gesture2)
        
    }
    
    @objc func openGenderPickerView(){
        showPicker(data: gender, title: "Gender".localized(), type: "gender")

    }
    @objc func openAgePickerView(){
        showPicker(data: age, title: "Age", type: "age")
    }

    func showPicker(data: [String], title: String, type: String){
        ActionSheetStringPicker.show(withTitle: title, rows: data, initialSelection: 0, doneBlock: {
            picker, index, value in
            switch type {
            case "gender":
                self.selectedGenderIndex(index: index)
            case "age":
                self.selectedAgeIndex(index: index)
            default:
                return
            }
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
    }
    
    func selectedGenderIndex(index: Int){
        print(gender[index])
        self.genderLabel.text = gender[index]
    }
    
    func selectedAgeIndex(index: Int){
        print(age[index])
        self.ageLabel.text = age[index]
    }
    
    
}


