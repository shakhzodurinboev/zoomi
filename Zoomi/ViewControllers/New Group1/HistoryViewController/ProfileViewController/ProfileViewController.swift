//
//  ProfileViewController.swift
//  zoomy
//
//  Created by ShakhzodUrinboev on 12/3/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    static let identifier = "ProfileViewController"

    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesign()
        setupTopImage(imageName: "logo")

    }


    func setupDesign(){
        self.imageView.layer.cornerRadius = self.imageView.frame.width / 2
        self.imageView.layer.masksToBounds = true
        
        
        self.view.backgroundColor = Theme.current.viewControllerBackgroundColor
        
    }

}
