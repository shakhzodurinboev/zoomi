//
//  HistoryViewController.swift
//  zoomy
//
//  Created by ShakhzodUrinboev on 12/3/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {
    static let identifier = "HistoryViewController"

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "History"
        setupTopImage(imageName: "logo")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = .clear
        self.view.backgroundColor = Theme.current.viewControllerBackgroundColor
        tableView.separatorStyle = .none
        
        let nib = UINib(nibName: HistoryTableViewCell.identifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: HistoryTableViewCell.identifier)
        
        tableView.rowHeight = 130
        
    }

}

extension HistoryViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: HistoryTableViewCell.identifier) as! HistoryTableViewCell
        cell.numberOfLikes.isHidden = true
        cell.favButton.isHidden = true
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        self.navigationController?.pushViewController(ProductViewController(), animated: true)
}
    
    
}
