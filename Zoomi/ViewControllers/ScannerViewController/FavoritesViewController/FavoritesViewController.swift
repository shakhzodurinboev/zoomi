//
//  FavoritesViewController.swift
//  zoomy
//
//  Created by ShakhzodUrinboev on 12/3/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController {
    static let identifier = "FavoritesViewController"

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Favorites"
        setupTopImage(imageName: "logo")
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 130
        let nib = UINib(nibName: HistoryTableViewCell.identifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: HistoryTableViewCell.identifier)
        self.view.backgroundColor = Theme.current.viewControllerBackgroundColor
        tableView.backgroundColor = .clear
        
        }

   
}
extension FavoritesViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: HistoryTableViewCell.identifier) as! HistoryTableViewCell
        cell.dateTime.isHidden = true
        return cell
    }
    
 
    
}

