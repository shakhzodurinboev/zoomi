//
//  BarcodeScannerViewController.swift
//  zoomy
//
//  Created by Mac on 12/29/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit
import BarcodeScanner
class BarcodeScannViewController: UIViewController {

    @IBOutlet weak var container: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let controller = storyboard!.instantiateViewController(withIdentifier: "BarcodeScan") as! ScannerViewController
        addChild(controller)
        container.addSubview(controller.view)
    }

}
