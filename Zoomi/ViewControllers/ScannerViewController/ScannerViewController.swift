//
//  ScannerViewController.swift
//  zoomy
//
//  Created by Mac on 12/29/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit
import BarcodeScanner
class ScannerViewController: BarcodeScannerViewController, BarcodeScannerCodeDelegate, BarcodeScannerErrorDelegate, BarcodeScannerDismissalDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        codeDelegate = self
        errorDelegate = self
        dismissalDelegate = self
        headerViewController.titleLabel.text = " "
        headerViewController.closeButton.setTitle("Закрыть", for: .normal)
        messageViewController.regularTintColor = .black
        messageViewController.errorTintColor = .red
        messageViewController.textLabel.textColor = .black
        messageViewController.messages.scanningText = "Поместите баркод в камеру. Сканирование начнётся автоматически."

        // Do any additional setup after loading the view.
    }

 

}
extension BarcodeScannerErrorDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        print(error)
    }
}
extension BarcodeScannerDismissalDelegate {
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
extension BarcodeScannerCodeDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        print(code)
        controller.reset()
    }
}
