//
//  ScannerViewController.swift
//  zoomy
//
//  Created by ShakhzodUrinboev on 12/23/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit

class Sc: UIViewController {

    @IBOutlet weak var select: UISegmentedControl!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var scannerView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTopImage(imageName: "logo")
        self.select.addTarget(self, action: #selector(selectAction), for: .touchUpInside)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func sendBtnTapped(_ sender: UIButton) {
    
    }
  
    @objc func selectAction(){
        if select.selectedSegmentIndex == 0{
            self.sendBtn.alpha = 0
            self.textField.alpha = 0
            self.scannerView.alpha = 1
        }else{
            UIView.animate(withDuration: 0.5) {
                self.sendBtn.alpha = 1
                self.textField.alpha = 1
                self.scannerView.alpha = 0
            }
        }
    }
    
    
    @IBAction func iconBtnTapped(_ sender: Any) {
        
    }
    
}
