//
//  HamburgerMenu.swift
//  zoomy
//
//  Created by ShakhzodUrinboev on 12/3/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit

protocol HamburgerMenuDelegate: class {
    func buttonSelected(with tag: Int)
}

class HamburgerMenu: UIViewController {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var zoomiLabel: UILabel!
    @IBOutlet weak var profileLabel: UIButton!
    @IBOutlet weak var historyLabel: UIButton!
    @IBOutlet weak var favoritesLabel: UIButton!
    @IBOutlet weak var commentsLabel: UIButton!
    @IBOutlet weak var shareLabel: UIButton!
    @IBOutlet weak var languageLabel: UIButton!
    @IBOutlet weak var aboutLabel: UIButton!
    
    weak var delegate: HamburgerMenuDelegate?
    
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        setupDesign()
    }
    func setupDesign(){
        iconImage.layer.cornerRadius = self.iconImage.frame.width / 2
        zoomiLabel.text = "Zoomi"
        
        profileLabel.setTitle("Profile", for: .normal)
        historyLabel.setTitle("History", for: .normal)
        favoritesLabel.setTitle("Favorites", for: .normal)
        commentsLabel.setTitle("Comments", for: .normal)
        shareLabel.setTitle("Share Zoomi", for: .normal)
        languageLabel.setTitle("Language", for: .normal)
        aboutLabel.setTitle("About", for: .normal)
        
        
        
    }
    @IBAction func buttonsAction(_ sender: UIButton) {
        delegate?.buttonSelected(with: sender.tag)
    }
    @IBAction func profileBtnAction(_ sender: UIButton) {
        delegate?.buttonSelected(with: sender.tag)

    }
    
    @IBAction func historyBtnAction(_ sender: UIButton) {
        delegate?.buttonSelected(with: sender.tag)

    }
    
    @IBAction func favoritesBtnAction(_ sender: UIButton) {
        delegate?.buttonSelected(with: sender.tag)

    }
    
    @IBAction func commentsBtnAction(_ sender: UIButton) {
        delegate?.buttonSelected(with: sender.tag)

    }
    
    @IBAction func shareBtnAction(_ sender: UIButton) {
        delegate?.buttonSelected(with: sender.tag)

    }
    
    @IBAction func languageBtnAction(_ sender: UIButton) {
        delegate?.buttonSelected(with: sender.tag)

    }
    @IBAction func aboutBtnAction(_ sender: UIButton) {
        delegate?.buttonSelected(with: sender.tag)

        
    }
}
