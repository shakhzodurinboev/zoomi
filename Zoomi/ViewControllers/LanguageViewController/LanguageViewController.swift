//
//  LanguageViewController.swift
//  zoomy
//
//  Created by ShakhzodUrinboev on 12/3/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit
import Localize_Swift
class LanguageViewController: UIViewController {
    static let identifier = "LanguageViewController"

    @IBOutlet weak var tableView: UITableView!
    var languageList = ["eng".localized(),"ru".localized(),"turk".localized(),"kazakh".localized(),"uzbek".localized()]


    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Language"
        setupTopImage(imageName: "logo")
        self.view.backgroundColor = Theme.current.viewControllerBackgroundColor
        tableView.backgroundColor = UIColor.clear
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 70
        let nib = UINib(nibName: LanguageTableViewCell.identifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: LanguageTableViewCell.identifier)
        print(Localize.availableLanguages())
        
        // Do any additional setup after loading the view.
    }
    
    
}
extension LanguageViewController:UITableViewDataSource,UITableViewDelegate{


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LanguageTableViewCell.identifier) as! LanguageTableViewCell
        cell.title.text = languageList[indexPath.row]
        if indexPath.row == 0{
            cell.button.isOn = true
        }else{
            cell.button.isOn = false
        }
       cell.button.addTarget(self, action: #selector(languageSelect), for: .touchUpInside)
        return cell
    }
    
    @objc func languageSelect(){
        Localize.setCurrentLanguage("uz")
      //  ["en", "kk", "Base", "tr", "ru", "uz"]
        tableView.reloadData()
    }
    
    
    

}

