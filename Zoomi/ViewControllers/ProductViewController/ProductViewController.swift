//
//  ProductViewController.swift
//  zoomy
//
//  Created by ShakhzodUrinboev on 12/15/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController {

    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var bucketButton: UIButton!
    @IBOutlet weak var contactButton: UIButton!
    @IBOutlet weak var siteButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        siteButton.imageView?.contentMode = .scaleAspectFit
        contactButton.imageView?.contentMode = .scaleAspectFit
        bucketButton.imageView?.contentMode = .scaleAspectFit
        infoButton.imageView?.contentMode = .scaleAspectFit
        
        }
    


   
    override func viewWillAppear(_ animated: Bool) {
        navigationItem.backBarButtonItem?.title = ""
    }
}
