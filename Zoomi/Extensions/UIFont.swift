//
//  UIFont.swift
//  zoomy
//
//  Created by ShakhzodUrinboev on 12/1/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit

/*
 SFUIDisplay-Regular
 SFUIDisplay-Bold
 SFUIDisplay-Thin
 SFUIDisplay-Medium
 SFUIDisplay-Ultralight
 SFUIDisplay-Semibold
 SFUIDisplay-Light
 */

extension UIFont {
    
    class func setArialNext(with size: CGFloat) -> UIFont? {
        return UIFont(name: "Avenir Next", size: size)
    }
    
}

enum SFUIDisplay: String {
    case Regular
    case Bold
    case Thin
    case Medium
    case Ultralight
    case Semibold
    case Light
    
    var uiFont: UIFont? {
        get {
            return UIFont(name: "SFUIDisplay-\(self.rawValue)", size: 15)
        }
    }
    
    var size10Font: UIFont {
        get {
            return UIFont(name: "SFUIDisplay-\(self.rawValue)", size: 10) ?? UIFont.init()
        }
    }
    var size12Font: UIFont {
        get {
            return UIFont(name: "SFUIDisplay-\(self.rawValue)", size: 12) ?? UIFont.init()
        }
    }
    
    var size13Font: UIFont {
        get {
            return UIFont(name: "SFUIDisplay-\(self.rawValue)", size: 13) ?? UIFont.init()
        }
    }
    
    var size14Font: UIFont {
        get {
            return UIFont(name: "SFUIDisplay-\(self.rawValue)", size: 14) ?? UIFont.init()
        }
    }
    
    var size15Font: UIFont {
        get {
            return UIFont(name: "SFUIDisplay-\(self.rawValue)", size: 15) ?? UIFont.init()
        }
    }
    var size16Font: UIFont {
        get{
            return UIFont(name: "SFUIDisplay-\(self.rawValue)", size: 16) ?? UIFont.init()
        }
    }
    
    var size17Font: UIFont {
        return UIFont(name: "SFUIDisplay-\(self.rawValue)", size: 17) ?? UIFont.init()
    }
    
    var size18Font: UIFont {
        get{
        return UIFont(name: "SFUIDisplay-\(self.rawValue)", size: 18) ?? UIFont.init()
        }
    }
    
    var size20Font: UIFont {
        return UIFont(name: "SFUIDisplay-\(self.rawValue)", size: 20) ?? UIFont.init()
    }
    
    var size22Font: UIFont {
        return UIFont(name: "SFUIDisplay-\(self.rawValue)", size: 22) ?? UIFont.init()
    }
    
    var size24Font: UIFont {
        return UIFont(name: "SFUIDisplay-\(self.rawValue)", size: 24) ?? UIFont.init()
    }
   
    
   
  
    
  
  
   
    
}
