//
//  UIViewController.swift
//  zoomy
//
//  Created by ShakhzodUrinboev on 12/3/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    func setIcons(rightBarButton: UIBarButtonItem, hamburgerMenuButton: UIBarButtonItem) {
        self.navigationController?.navigationBar.tintColor = UIColor.short(red: 55, green: 55, blue: 55)
        self.navigationBar.topItem?.rightBarButtonItem = rightBarButton
        self.navigationBar.topItem?.leftBarButtonItem = hamburgerMenuButton
    }
    
}


