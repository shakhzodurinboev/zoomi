//
//  Theme.swift
//  zoomy
//
//  Created by ShakhzodUrinboev on 12/1/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import Foundation
import UIKit

enum Theme: Int {
    case light, dark
    
    private enum Keys {
        static let selectedTheme = "SelectedTheme"
    }
    
    static var current: Theme {
        let storedTheme = UserDefaults.standard.integer(forKey: Keys.selectedTheme)
        return Theme(rawValue: storedTheme) ?? .light
    }
    
    var mainColor: UIColor {
        return .white
    }
    
    var barStyle: UIBarStyle {
        switch self {
        case .light:
            return .default
        case .dark:
            return .black
        }
    }
    
    var backgroundColor: UIColor {
        switch self {
        case .light:
            return UIColor.colorFromHexString("f5f5f5")
        case .dark:
            return UIColor(white: 0.4, alpha: 1.0)
        }
    }
    
    var boxBorderColor: UIColor {
        return UIColor.colorFromHexString("a1a1a1")
    }
    
    var tintColor: UIColor {
        return UIColor.colorFromHexString("0e427a")
    }
    
    var navigationTintColor: UIColor {
        return UIColor.white
    }
    var navigationBarTintColor: UIColor {
        return UIColor.short(red: 248, green: 250, blue: 251)
    }
    var lightGreyBlue: UIColor {
        return UIColor.short(red: 198, green: 215, blue: 230)
    }
    var errorRed: UIColor {
        return UIColor.short(red: 239, green: 48, blue: 79)
    }
    var backgroundTabBarColor: UIColor {
        return UIColor.short(red: 239, green: 246, blue: 252)
    }
    var skipViewBorderColor: UIColor {
        return UIColor.short(red: 31, green: 99, blue: 165)
    }
    var unselectedTabBarColor: UIColor {
        return UIColor.short(red: 148, green: 182, blue: 210)
    }
    var viewControllerBackgroundColor: UIColor {
        return UIColor.short(red: 248, green: 250, blue: 251)
    }
    
    var appColor:UIColor{
        return UIColor.short(red: 30, green: 150, blue: 80)
    }
    
    var appRedColor:UIColor{
        return UIColor.short(red: 161, green: 45, blue: 46)
    }
    var mainPinkColor:UIColor{
        return UIColor.short(red: 232, green: 68, blue: 133)
    }
    
    func apply() {
        UserDefaults.standard.set(rawValue, forKey: Keys.selectedTheme)
        UserDefaults.standard.synchronize()
        UIApplication.shared.delegate?.window??.tintColor = mainColor
        let navigationBar = UINavigationBar.appearance()
        navigationBar.isTranslucent = false
        navigationBar.tintColor = navigationTintColor
        navigationBar.barStyle = barStyle
        navigationBar.barTintColor = navigationBarTintColor
        navigationBar.shadowImage = UIImage()
        navigationBar.setBackgroundImage(UIImage(), for: .default)
      
        
        
        /* Style for tab bar when appearance*/
        let tabBarItemAppearance = UITabBarItem.appearance()
        let tabBarItemAttributes = [NSAttributedString.Key.font:SFUIDisplay.Regular.size10Font]
        tabBarItemAppearance.setTitleTextAttributes(tabBarItemAttributes, for: .normal)
        
        let tabBar = UITabBar.appearance()
        tabBar.isTranslucent = false
        tabBar.tintColor = skipViewBorderColor
        if #available(iOS 10.0, *) {
            tabBar.unselectedItemTintColor = unselectedTabBarColor
        }
        //        tabBar.barStyle = barStyle
        tabBar.backgroundColor = backgroundTabBarColor
        tabBar.barTintColor = backgroundTabBarColor
        
    
        
    }
}
