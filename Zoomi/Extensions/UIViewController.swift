//
//  UIViewController.swift
//  zoomy
//
//  Created by ShakhzodUrinboev on 12/3/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit

extension UIViewController{
    
    
    func setupTopImage(imageName:String){
        let image: UIImage = UIImage(named: imageName)!
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        self.navigationItem.titleView = imageView
    }
}
