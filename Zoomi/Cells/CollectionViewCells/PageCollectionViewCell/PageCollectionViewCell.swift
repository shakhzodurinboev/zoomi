//
//  PageCollectionViewCell.swift
//  zoomy
//
//  Created by ShakhzodUrinboev on 12/10/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit

class PageCollectionViewCell: UICollectionViewCell {
    
static let identifier = "PageCollectionViewCell"
    
    var page:Page?{
        didSet{
            guard let unwrappedPage = page else {return}
            
            bearImageView.image = UIImage(named: unwrappedPage.imageName)
            let attributedText = NSMutableAttributedString(string: (unwrappedPage.bodyText), attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16),NSAttributedString.Key.foregroundColor:UIColor.gray])
            
            descriptionTextView.attributedText = attributedText
            descriptionTextView.textAlignment = .center
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLayout()
        
    }
    private let bearImageView:UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "logo1"))
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    private let descriptionTextView:UITextView = {
        let textView = UITextView()
        let attributedText = NSMutableAttributedString(string: "Join us today is our fun and games", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16),NSAttributedString.Key.foregroundColor:UIColor.gray])
        
        textView.attributedText = attributedText
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.textAlignment = .center
        textView.isSelectable = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    let boxView:UIView={
       let view = UIView()
        view.backgroundColor = .white
        return view
        
    }()
    func setupLayout(){
        addSubview(boxView)
        addSubview(descriptionTextView)
        
        boxView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), size: CGSize(width: 0, height: 0))
     
        boxView.addSubview(bearImageView)
        bearImageView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: UIEdgeInsets(top: -70, left: 0, bottom: 70, right: 0), size: CGSize(width: 0, height: 0))
        
        descriptionTextView.leftAnchor.constraint(equalTo: leftAnchor, constant: 24).isActive = true
        descriptionTextView.rightAnchor.constraint(equalTo:rightAnchor, constant: -24).isActive = true
        descriptionTextView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -30).isActive = true
        descriptionTextView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        
    }
}
