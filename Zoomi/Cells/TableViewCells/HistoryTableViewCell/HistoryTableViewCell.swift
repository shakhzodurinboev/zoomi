//
//  HistoryTableViewCell.swift
//  zoomy
//
//  Created by ShakhzodUrinboev on 12/15/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {
    static let identifier = "HistoryTableViewCell"

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var numberOfLikes: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        self.backgroundColor = UIColor.clear
        // Configure the view for the selected state
    }
    
}
