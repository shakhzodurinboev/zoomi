//
//  LanguageTableViewCell.swift
//  zoomy
//
//  Created by ShakhzodUrinboev on 12/20/18.
//  Copyright © 2018 ShakhzodUrinboev. All rights reserved.
//

import UIKit

class LanguageTableViewCell: UITableViewCell {
    static let identifier = "LanguageTableViewCell"

    
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var button: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        button.isOn = false
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
            
    }
    
}
